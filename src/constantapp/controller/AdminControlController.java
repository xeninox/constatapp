/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.controller;

import constantapp.model.User;
import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.AdminBilling;
import constantapp.view.AdminControl;
import constantapp.view.AdminDisconnect;
import constantapp.view.CustomerList;
import constantapp.view.HomeView;
import constantapp.view.MassDisconnect;
import constantapp.view.SearchDefaulters;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Constant
 */
public class AdminControlController implements ActionListener {

    /**
     * Instance varibles
     */
    AdminControl controlPage;
    UserPaymentModel uPayModel;
    UserModel user;

    /**
     * Constructor for AdminControlController
     *
     * @param controlPage Admin control page
     * @param uPayModel The user payment model
     * @param uModel The user model
     */
    public AdminControlController(AdminControl controlPage, UserPaymentModel uPayModel, UserModel uModel) {
        this.controlPage = controlPage;
        this.uPayModel = uPayModel;
        this.user = uModel;
    }

    public void handleUIControls() {
        controlPage.getBack().addActionListener(this);
        controlPage.getDisconeectUser().addActionListener(this);
        controlPage.getDisconnectAll().addActionListener(this);
        controlPage.getBillUser().addActionListener(this);
        controlPage.getAllCustomers().addActionListener(this);
        controlPage.getLogOut().addActionListener(this);
        controlPage.getConnected().addActionListener(this);
        controlPage.getDisconnected().addActionListener(this);
        controlPage.getSearch().addActionListener(this);
        controlPage.genList().addActionListener(this);
        controlPage.setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == controlPage.getBack()) {
            HomeView hv = new HomeView();
            HomeViewController hvc = new HomeViewController(user, hv, uPayModel);
            hvc.handleUIControls();
            controlPage.setVisible(false);
        } else if (e.getSource() == controlPage.getBillUser()) {
            AdminBilling ab = new AdminBilling();
            AdminBillingController abc = new AdminBillingController(ab, uPayModel, user);
            abc.handleUIControls();
            controlPage.setVisible(false);
        } else if (e.getSource() == controlPage.getDisconeectUser()) {
            AdminDisconnect ad = new AdminDisconnect();
            AdminDisconnectController adc = new AdminDisconnectController(ad, uPayModel, user);
            adc.handleUIControls();
            controlPage.setVisible(false);
        } else if (e.getSource() == controlPage.getDisconnectAll()) {
            MassDisconnect ad = new MassDisconnect();
            MassDisconnectController adc = new MassDisconnectController(ad, uPayModel, user);
            adc.handleUIControls();
            controlPage.setVisible(false);
        } else if (e.getSource() == controlPage.getAllCustomers()) {
            CustomerList cl = new CustomerList();
            CustomerListController clc = new CustomerListController(cl, uPayModel, user);
            clc.handleUIControls();
            controlPage.setVisible(false);
        } else if (e.getSource() == controlPage.getLogOut()) {
            HomeView hv = new HomeView();
            HomeViewController hvc = new HomeViewController(user, hv, uPayModel);
            hvc.handleUIControls();
            controlPage.setVisible(false);
        } else if (e.getSource() == controlPage.getSearch()) {
            SearchDefaulters dv = new SearchDefaulters();
            if (controlPage.getConnected().isSelected()) {
                SearchDefaultersController sdc = new SearchDefaultersController(dv, uPayModel, user, "connected");
                sdc.handleUIControls();
            } else if (controlPage.getDisconnected().isSelected()) {
                SearchDefaultersController sdc = new SearchDefaultersController(dv, uPayModel, user, "disconnected");
                sdc.handleUIControls();
            }
            controlPage.setVisible(false);
        } else if (e.getSource() == controlPage.genList()) {
            File file = new File("CustomerDetails.txt");
            PrintWriter pw = null;
            try {
                FileWriter fr = new FileWriter(file, true);
                pw = new PrintWriter(fr);
                pw.println("USERNAME" + "\t\t" + "DECODER NUMBER");
                ArrayList<User> allUsers = user.getUsers();
                for (User u : allUsers) {
                    pw.println(u.getUsername() + "\t\t" + u.getDecoderNum());
                }

                pw.println("Number of users: " + allUsers.size());
                JOptionPane.showMessageDialog(null, "Report generated successfully");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "An error occured when trying to save student details");
            } finally {
                if (pw != null) {
                    pw.close();
                }
            }
        }
    }

}
