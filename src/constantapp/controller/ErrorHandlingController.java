/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.controller;

/**This class controls
 *
 * @author Constant
 */
public class ErrorHandlingController {

    /**
     * 
     * @param input
     * @return boolean
     */
    public boolean isAlphabetic(String input) {
        return input.matches("[a-zA-Z_]+");
    }

    /**
     * 
     * @param input
     * @return 
     */
    public boolean isNumeric(String input) {
        return input.matches("[0-9]+");
    }

    /**
     * 
     * @param input
     * @return 
     */
    public boolean isDecimal(String input) {
        return input.matches("^[1-9]\\d*(\\.\\d+)?$");
    }
}
