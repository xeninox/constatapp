/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.controller;

import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.HomeView;
import constantapp.view.RegisterView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Constant
 */
public class RegisterController implements ActionListener {

    UserModel uModel;
    RegisterView regView;
    UserPaymentModel uPayModel;

    /**
     * Constructor for RegisterController
     *
     * @param uModel user model
     * @param regView register view
     * @param uPayModel user payment model
     */
    public RegisterController(UserModel uModel, RegisterView regView, UserPaymentModel uPayModel) {
        this.uModel = uModel;
        this.regView = regView;
        this.uPayModel = uPayModel;
    }

    /**
     * Method to initialize the buttons
     *
     * @return void
     */
    public void handleUIControls() {
        regView.register().addActionListener(this);
        regView.back().addActionListener(this);
        regView.setVisible(true);
    }

    /**
     * Method to handle all action listeners
     *
     * @param e
     * @return void
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == regView.register()) {
            String username = regView.getUsername().getText();
            String decoderNum = regView.getDecoderNum().getText();
            String password = regView.getPassword().getText();
            String role = "user";
            if (!username.isEmpty() && !decoderNum.isEmpty() && !password.isEmpty()) {
                uModel.registerUser(username, decoderNum, password, role);
                UserPaymentModel uPayModel = UserPaymentModel.getInstance();
                uPayModel.setDecoderStatus(decoderNum, "connected");
            } else {
                JOptionPane.showConfirmDialog(null,
                        "Some values are in the wrong format", "Failure!",
                        JOptionPane.CANCEL_OPTION);
            }
        } else if (e.getSource() == regView.back()) {
            HomeView hv = new HomeView();
            HomeViewController hvc = new HomeViewController(uModel, hv, uPayModel);
            regView.setVisible(false);
            hvc.handleUIControls();
        }
    }

}
