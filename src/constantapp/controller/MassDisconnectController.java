/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.controller;

import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.AdminControl;
import constantapp.view.HomeView;
import constantapp.view.MassDisconnect;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Constant
 */
public class MassDisconnectController implements ActionListener {

    MassDisconnect discPage;
    UserPaymentModel uPayModel;
    UserModel user;

    /**
     * Constructor for MassDisconnectController
     *
     * @param discPage disconnect page
     * @param uPayModel user payment model
     * @param user user model
     */
    public MassDisconnectController(MassDisconnect discPage, UserPaymentModel uPayModel, UserModel user) {
        this.discPage = discPage;
        this.uPayModel = uPayModel;
        this.user = user;
    }

    /**
     * Method to initialize the buttons
     *
     * @return void
     */
    public void handleUIControls() {
        discPage.getBack().addActionListener(this);
        discPage.getDisconnect().addActionListener(this);
        discPage.getLogOut().addActionListener(this);
        discPage.setVisible(true);
    }

    /**
     * Method to handle all action listeners
     *
     * @param e
     * @return void
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == discPage.getBack()) {
            AdminControl ac = new AdminControl();
            AdminControlController acc = new AdminControlController(ac, uPayModel, user);
            acc.handleUIControls();
            discPage.setVisible(false);
        } else if (e.getSource() == discPage.getDisconnect()) {
            ErrorHandlingController ehc = new ErrorHandlingController();
            String amt = discPage.getAmount().getText();
            if (ehc.isNumeric(amt) && !amt.isEmpty()) {
                double amount = Double.parseDouble(amt);
                uPayModel.massDisconnect(amount);
            } else {
                JOptionPane.showMessageDialog(null,
                        "Please make sure that the least amount is in the right format");
            }
        } else if (e.getSource() == discPage.getLogOut()) {
            HomeView hv = new HomeView();
            HomeViewController hvc = new HomeViewController(user, hv, uPayModel);
            hvc.handleUIControls();
            discPage.setVisible(false);
        }
    }

}
