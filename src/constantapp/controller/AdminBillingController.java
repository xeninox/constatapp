package constantapp.controller;


import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.AdminBilling;
import constantapp.view.AdminControl;
import constantapp.view.HomeView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Constant
 */
public class AdminBillingController implements ActionListener {

    /**
     * Instance variables
     */
    AdminBilling billView;
    UserPaymentModel uPayModel;
    UserModel uModel;

    /**
     * Constructor for AdminBillingController
     *
     * @param billView The view for the bill page
     * @param uPayModel The user payment model
     * @param uModel The user model
     */
    public AdminBillingController(AdminBilling billView, UserPaymentModel uPayModel, UserModel uModel) {
        this.billView = billView;
        this.uPayModel = uPayModel;
        this.uModel = uModel;
    }

    public void handleUIControls() {
        billView.getBack().addActionListener(this);
        billView.getCharge().addActionListener(this);
        billView.getLogout().addActionListener(this);
        billView.setVisible(true);
    }

   
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == billView.getBack()) {
            AdminControl ac = new AdminControl();
            AdminControlController acc = new AdminControlController(ac, uPayModel, uModel);
            acc.handleUIControls();
            billView.setVisible(false);
        } else if (e.getSource() == billView.getCharge()) {
            String decoderNum = billView.getDecNum().getText();
            String month = billView.getMonth().getSelectedItem().toString();
            String bill = billView.getBill().getText();
            ErrorHandlingController ehc = new ErrorHandlingController();
            if (!decoderNum.isEmpty() && !month.isEmpty() && !bill.isEmpty()) {
                if (ehc.isNumeric(bill)) {
                    double bill1 = Double.parseDouble(bill);
                    uPayModel.billUser(decoderNum, month, bill1);
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Please make sure that the bill is in the right format");
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "Please make sure that all fields are filled");
            }
        } else if (e.getSource() == billView.getLogout()) {
            HomeView hv = new HomeView();
            HomeViewController hvc = new HomeViewController(uModel, hv, uPayModel);
            hvc.handleUIControls();
            billView.setVisible(false);
        }
    }

}
