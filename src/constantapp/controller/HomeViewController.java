/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.controller;

import constantapp.model.User;
import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.AdminControl;
import constantapp.view.HomeView;
import constantapp.view.RegisterView;
import constantapp.view.UserPayment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Constant
 */
public class HomeViewController implements ActionListener {

    UserModel uModel;
    HomeView homeView;
    UserPaymentModel uPayModel;

    /**
     * Constructor for HomeViewController
     *
     * @param uModel the user model
     * @param homeView the home view
     * @param uPayModel the user payment model
     */
    public HomeViewController(UserModel uModel, HomeView homeView, UserPaymentModel uPayModel) {
        this.uModel = uModel;
        this.homeView = homeView;
        this.uPayModel = uPayModel;
    }

    /**
     * Method to initialize the buttons
     *
     * @return void
     */
    public void handleUIControls() {
        homeView.getLogin().addActionListener(this);
        homeView.getRegister().addActionListener(this);
        homeView.setVisible(true);
    }

    /**
     * Method to handle all action listeners
     *
     * @param e
     * @return void
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == homeView.getRegister()) {
            RegisterView rgView = new RegisterView();
            RegisterController regController = new RegisterController(uModel, rgView, uPayModel);
            homeView.setVisible(false);
            regController.handleUIControls();
        } else if (e.getSource() == homeView.getLogin()) {
            String username = homeView.getUsername().getText();
            String password = homeView.getPassword().getText();
            User authUser = uModel.authnticateUser(username, password);
            if (!username.isEmpty() && !password.isEmpty()) {
                if (authUser == null) {
                    JOptionPane.showMessageDialog(null,
                            "Username or password incorrect");
                } else if (authUser.getRole().equalsIgnoreCase("user")) {
                    UserPayment paymentView = new UserPayment();
                    UserPaymentController upc = new UserPaymentController(uPayModel, uModel, paymentView, authUser);
                    upc.handleUIControls();
                    homeView.setVisible(false);
                } else if (authUser.getRole().equalsIgnoreCase("admin")) {
                    AdminControl controlView = new AdminControl();
                    AdminControlController upc = new AdminControlController(controlView, uPayModel, uModel);
                    upc.handleUIControls();
                    homeView.setVisible(false);
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "Please fill in your details");
            }

        }
    }

}
