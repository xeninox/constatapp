/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.controller;

import constantapp.model.DefaultingUsers;
import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.AdminControl;
import constantapp.view.SearchDefaulters;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Constant
 */
public class SearchDefaultersController implements ActionListener {

    SearchDefaulters defaulterView;
    UserPaymentModel uPayModel;
    UserModel uModel;

    /**
     * Constructor for SearchDefaultersController
     *
     * @param defaulterView defaulter view
     * @param uPayModel user payment model
     * @param uModel user model
     * @param mode user's mode whether connected or disconnected
     */
    public SearchDefaultersController(SearchDefaulters defaulterView, UserPaymentModel uPayModel, UserModel uModel, String mode) {
        DefaultingUsers def = new DefaultingUsers(mode);
        this.defaulterView = defaulterView;
        this.uPayModel = uPayModel;
        this.uModel = uModel;
        defaulterView.getTable().setModel(def);
    }

    /**
     * Method to initialize the buttons
     *
     * @return void
     */
    public void handleUIControls() {
        defaulterView.getBack().addActionListener(this);
        defaulterView.setVisible(true);
    }

    /**
     * Method to handle all action listeners
     *
     * @param e
     * @return void
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == defaulterView.getBack()) {
            AdminControl ac = new AdminControl();
            AdminControlController acc = new AdminControlController(ac, uPayModel, uModel);
            acc.handleUIControls();
            defaulterView.setVisible(false);
        }
    }

}
