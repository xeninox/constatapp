/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.controller;

import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.AdminControl;
import constantapp.view.CustomerList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Constant
 */
public class CustomerListController implements ActionListener {

    CustomerList custView;
    UserPaymentModel uPayModel;
    UserModel uModel;

    /**
     * Constructor for CustomerListController
     *
     * @param custView the customer view
     * @param uPayModel the user payment model
     * @param uModel the user model
     */
    public CustomerListController(CustomerList custView, UserPaymentModel uPayModel, UserModel uModel) {
        this.custView = custView;
        this.uPayModel = uPayModel;
        this.uModel = uModel;
        custView.getTable().setModel(uModel);
    }

    /**
     * Method to initialize the buttons
     *
     * @return void
     */
    public void handleUIControls() {
        custView.getBack().addActionListener(this);
        custView.getDelete().addActionListener(this);
        custView.setVisible(true);
    }

    /**
     * Method to handle all action listeners
     *
     * @param e
     * @return void
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == custView.getBack()) {
            AdminControl ac = new AdminControl();
            AdminControlController acc = new AdminControlController(ac, uPayModel, uModel);
            acc.handleUIControls();
            custView.setVisible(false);
        } else if (e.getSource() == custView.getDelete()) {
            int row = custView.getTable().getSelectedRow();
            uModel.removeRow(row);
            custView.getTable();
        }
    }

}
