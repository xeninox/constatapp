/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.controller;

import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.AdminControl;
import constantapp.view.AdminDisconnect;
import constantapp.view.HomeView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Constant
 */
public class AdminDisconnectController implements ActionListener {

    AdminDisconnect disconnectPage;
    UserPaymentModel uPayModel;
    UserModel user;

    /**
     * Constructor for AdminDisconnectController
     *
     * @param disconnectPage the disconnect view
     * @param uPayModel the user payment model
     * @param user the user model
     */
    public AdminDisconnectController(AdminDisconnect disconnectPage, UserPaymentModel uPayModel, UserModel user) {
        this.disconnectPage = disconnectPage;
        this.uPayModel = uPayModel;
        this.user = user;
    }

    /**
     * Method to initialize the buttons
     *
     * 
     */
    public void handleUIControls() {
        disconnectPage.getBack().addActionListener(this);
        disconnectPage.getLogOut().addActionListener(this);
        disconnectPage.getDisconnect().addActionListener(this);
        disconnectPage.getConnect().addActionListener(this);
        disconnectPage.setVisible(true);
    }

    /**
     * Method to handle all action listeners
     *
     * 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == disconnectPage.getBack()) {
            AdminControl ac = new AdminControl();
            AdminControlController acc = new AdminControlController(ac, uPayModel, user);
            acc.handleUIControls();
            disconnectPage.setVisible(false);
        } else if (e.getSource() == disconnectPage.getDisconnect()) {
            String decNum = disconnectPage.getDecNum().getText();
            if (!decNum.isEmpty()) {
                uPayModel.updateDecoderStatus(decNum, "disconnected");
            } else {
                JOptionPane.showMessageDialog(null,
                        "Please make sure that all fields are filled");
            }
        } else if (e.getSource() == disconnectPage.getConnect()) {
            String decNum = disconnectPage.getDecNum().getText();
            if (!decNum.isEmpty()) {
                uPayModel.updateDecoderStatus(decNum, "connected");
            } else {
                JOptionPane.showMessageDialog(null,
                        "Please make sure that all fields are filled");
            }
        } else if (e.getSource() == disconnectPage.getLogOut()) {
            HomeView hv = new HomeView();
            HomeViewController hvc = new HomeViewController(user, hv, uPayModel);
            hvc.handleUIControls();
            disconnectPage.setVisible(false);
        }
    }

}
