/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.controller;

import constantapp.model.User;
import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.HomeView;
import constantapp.view.UserPayment;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Constant
 */
public class UserPaymentController implements ActionListener {

    UserPaymentModel uPayModel;
    UserModel uModel;
    UserPayment paymentView;
    User user;

    /**
     * Constructor for UserPaymentController
     *
     * @param uPayModel the user payment model
     * @param uModel the user model
     * @param paymentView the payment view
     * @param user the user object
     */
    public UserPaymentController(UserPaymentModel uPayModel, UserModel uModel, UserPayment paymentView, User user) {
        this.uPayModel = uPayModel;
        this.uModel = uModel;
        this.paymentView = paymentView;
        this.user = user;
    }

    /**
     * Method to initialize the buttons
     *
     * @return void
     */
    public void handleUIControls() {
        paymentView.getBack().addActionListener(this);
        paymentView.getPay().addActionListener(this);
        paymentView.getLogOut().addActionListener(this);
        paymentView.setVisible(true);
        update();
    }

    /**
     * Method to handle all action listeners
     *
     * @param e
     * @return void
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == paymentView.getBack()) {
            HomeView hv = new HomeView();
            HomeViewController hvc = new HomeViewController(uModel, hv, uPayModel);
            paymentView.setVisible(false);
            hvc.handleUIControls();
        } else if (e.getSource() == paymentView.getPay()) {
            String decoderNum = paymentView.getDecNum().getText();
            String month = paymentView.getMonth().getSelectedItem().toString();
            String amt = paymentView.getAmount().getText();
            ErrorHandlingController ehc = new ErrorHandlingController();
            if (!decoderNum.isEmpty() && !month.isEmpty() && !amt.isEmpty()) {
                if (ehc.isNumeric(amt)) {
                    double amount = Double.parseDouble(amt);
                    uPayModel.payBill(decoderNum, month, amount);
                    update();
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Please make sure that the amount to be paid is in the right format");
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "Please make sure all fields are filled");
            }
        } else if (e.getSource() == paymentView.getLogOut()) {
            HomeView hv = new HomeView();
            HomeViewController hvc = new HomeViewController(uModel, hv, uPayModel);
            hvc.handleUIControls();
            paymentView.setVisible(false);
        }
    }

    public void update() {
        double amountOwing = uPayModel.getTotalCharge(user.getDecoderNum())
                - uPayModel.getTotalPaid(user.getDecoderNum());
        if (amountOwing <= 0) {
            paymentView.setAmtOwing(Double.toString(0));
        } else {
            paymentView.setAmtOwing(Double.toString(amountOwing));
        }
        String status = uPayModel.getDecoderStatus(user.getDecoderNum());
        if (status != null) {
            if (status.equalsIgnoreCase("connected")) {
                paymentView.getStatusLabel().setForeground(Color.YELLOW);
            }
            paymentView.setStatus(status.toUpperCase());
        } else {
            paymentView.setStatus("UNKNOWN");
        }
    }

}
