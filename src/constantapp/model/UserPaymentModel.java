/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Constant
 */
public class UserPaymentModel {

    static UserPaymentModel theModel = null;

    public UserPaymentModel() {
    }

    public static UserPaymentModel getInstance() {
        if (theModel == null) {
            theModel = new UserPaymentModel();
        }
        return theModel;
    }

    /**
     * Method for a user to pay their bills
     *
     * @param decoder_num the decoder number
     * @param pay_month the month for which the user is paying
     * @param payment_amt the payment amount
     */
    public void payBill(String decoder_num, String pay_month, double payment_amt) {

        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            PreparedStatement p = conn.prepareStatement("INSERT INTO Payment SET"
                    + " decoder_num=?, pay_month=? ,payment_amt =?");
            p.setString(1, decoder_num);
            p.setString(2, pay_month);
            p.setDouble(3, payment_amt);
            p.execute();

            //userRecords.add(new User(username, decoderNum, password, role));
            JOptionPane.showMessageDialog(null,
                    "Amount paid successfully");

        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * Method to get the total amount a user is owing
     *
     * @param decNum the decoder number
     * @return the total amount a user is owing
     */
    public double getTotalCharge(String decNum) {
        double amount = 0;
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            PreparedStatement p = conn.prepareStatement("SELECT SUM(bill) FROM Charge WHERE decoder_num=?");
            p.setString(1, decNum);

            ResultSet result = p.executeQuery();
            result.next();
            String sum = result.getString(1);
            if (sum == null) {
                amount = 0;
            } else {
                amount = Double.parseDouble(sum);
            }
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
        return amount;
    }

    /**
     * Method to get the total amount of money a person has paid
     *
     * @param decNum the decoder number
     * @return the total amount paid by a user
     */
    public double getTotalPaid(String decNum) {
        double amount = 0;
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            PreparedStatement p = conn.prepareStatement("SELECT SUM(payment_amt) FROM Payment WHERE decoder_num=?");
            p.setString(1, decNum);

            ResultSet result = p.executeQuery();
            result.next();
            String sum = result.getString(1);
            if (sum == null) {
                amount = 0;
            } else {
                amount = Double.parseDouble(sum);
            }

        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
        return amount;
    }

    /**
     * Method to change the status of a decoder
     *
     * @param decNum the decoder number
     * @param status the status of the decoder
     */
    public void setDecoderStatus(String decNum, String status) {
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            PreparedStatement p = conn.prepareStatement("INSERT INTO Decoder_Status SET"
                    + " decoder_num=?, decoder_status=?");
            p.setString(1, decNum);
            p.setString(2, status);
            p.execute();
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * Method to update the status of a decoder
     *
     * @param decNum the decoder number
     * @param status the status of the decoder
     */
    public void updateDecoderStatus(String decNum, String status) {
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");
            PreparedStatement p = conn.prepareStatement("UPDATE Decoder_Status SET "
                    + "decoder_status=? WHERE decoder_num =?");
            p.setString(2, decNum);
            p.setString(1, status);
            p.execute();

            JOptionPane.showMessageDialog(null,
                    "Decoder has been successfully " + status);
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * Method to update the status of a decoder
     *
     * @param decNum the decoder number
     * @param status the status of the decoder
     */
    public void updateDecoderStatus1(String decNum, String status) {
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");
            PreparedStatement p = conn.prepareStatement("UPDATE Decoder_Status SET "
                    + "decoder_status=? WHERE decoder_num =?");
            p.setString(2, decNum);
            p.setString(1, status);
            p.execute();
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * Method to get the decoder status
     *
     * @param decNum the decoder number
     * @return the status of the decoder
     */
    public String getDecoderStatus(String decNum) {
        String status = null;
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            PreparedStatement p = conn.prepareStatement("SELECT decoder_status FROM Decoder_Status WHERE decoder_num=?");
            p.setString(1, decNum);

            ResultSet result = p.executeQuery();
            result.next();
            status = result.getString(1);
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
        return status;
    }

    /**
     * Method to bil a user
     *
     * @param decoder_num the decoder number
     * @param month the month for which the bill is to be paid
     * @param bill the amount to be paid
     */
    public void billUser(String decoder_num, String month, double bill) {

        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            PreparedStatement p = conn.prepareStatement("INSERT INTO Charge SET"
                    + " decoder_num=?, charge_month=? ,bill =?");
            p.setString(1, decoder_num);
            p.setString(2, month);
            p.setDouble(3, bill);
            p.execute();

            //userRecords.add(new User(username, decoderNum, password, role));
            JOptionPane.showMessageDialog(null,
                    "Amount billed successfully");

        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * Method to disconnect a large amount of people
     *
     * @param amt the least amount that a person should owe
     */
    public void massDisconnect(double amt) {
        ArrayList<String> decNums = new ArrayList<>();
        ArrayList<Double> charges = new ArrayList<>();
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            PreparedStatement p = conn.prepareStatement("SELECT sum(bill) - SUM(payment_amt) AS Answer, Payment.decoder_num FROM Payment, Charge GROUP BY (Payment.decoder_num)");
            ResultSet result = p.executeQuery();
            while (result.next()) {
                decNums.add(result.getString(2));
                charges.add(result.getDouble(1));
            }

            for (int i = 0; i < decNums.size(); i++) {
                if (charges.get(i) >= amt) {
                    System.out.println(decNums.get(i) + " " + charges.get(i));
                    updateDecoderStatus1(decNums.get(i), "disconnected");
                }
            }
            JOptionPane.showMessageDialog(null,
                    "Mass disconnection successful");
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
    }

}
