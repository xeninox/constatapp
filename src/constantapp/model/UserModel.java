/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Constant
 */
public class UserModel extends AbstractTableModel {

    private final String[] columnNames;
    private ArrayList<User> userRecords;
    static UserModel theModel = null;

    /**
     * The user model constructor
     */
    public UserModel() {
        userRecords = new ArrayList<>();
        columnNames = new String[]{"Username", "Decoder Number"};
        fetchTableData();
    }

    public static UserModel getInstance() {
        if (theModel == null) {
            theModel = new UserModel();
        }
        return theModel;
    }

    @Override
    public int getRowCount() {
        return userRecords.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        User user = new User();
        switch (columnIndex) {
            case 0:
                user.setUsername((String) value);
                break;
            case 1:
                user.setDecoderNum((String) value);
                break;
            case 2:
                user.setPassword((String) value);
                break;
            case 3:
                user.setRole((String) value);
                break;
            default:
                break;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return userRecords.get(rowIndex).getUsername();
            case 1:
                return userRecords.get(rowIndex).getDecoderNum();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        try {
            return columnNames[columnIndex];
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Retrieve data from the database
     *
     */
    private void fetchTableData() {
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            Statement s = conn.createStatement();

            ResultSet rs = s.executeQuery("SELECT username, decoder_num FROM Customer");

            String username;
            String decoderNum;

            while (rs.next()) {
                username = rs.getString(1);
                decoderNum = rs.getString(2);

                userRecords.add(new User(username, decoderNum, null, null));
            }
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            JOptionPane.showMessageDialog(null, e
                    + "\nThere is a problem with the database connection or query");
            System.exit(0);
        }
    }

    /**
     * Method to retrieve all users from the database
     *
     * @return an array list of all users
     */
    private ArrayList<User> getCustDetails() {
        ArrayList<User> myUser = new ArrayList<>();
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            Statement s = conn.createStatement();

            ResultSet rs = s.executeQuery("SELECT username, decoder_num FROM Customer");

            String username;
            String decoderNum;

            while (rs.next()) {
                username = rs.getString(1);
                decoderNum = rs.getString(2);

                myUser.add(new User(username, decoderNum, null, null));
            }
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            JOptionPane.showMessageDialog(null, e
                    + "\nThere is a problem with the database connection or query");
            System.exit(0);
        }
        return myUser;
    }

    /**
     * Method to register a user
     *
     * @param username the username of the user
     * @param decoderNum the decoder number
     * @param password password of the user
     * @param role the role of the user
     */
    public void registerUser(String username, String decoderNum, String password, String role) {

        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            PreparedStatement p = conn.prepareStatement("INSERT INTO Customer SET"
                    + " username=?, decoder_num=? ,user_pass =?, role =?");
            p.setString(1, username);
            p.setString(2, decoderNum);
            p.setString(3, password);
            p.setString(4, role);
            p.execute();

            userRecords.add(new User(username, decoderNum, password, role));

            JOptionPane.showMessageDialog(null,
                    "User has been added successfully");

        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * Method to retrieve all users
     *
     * @return an arraylist of all users
     */
    public ArrayList<User> getUsers() {
        ArrayList<User> allUsers = new ArrayList<>();
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            Statement s = conn.createStatement();

            ResultSet rs = s.executeQuery("SELECT username, decoder_num, user_pass, role FROM Customer");

            String username;
            String decoderNum;
            String role;

            while (rs.next()) {
                username = rs.getString(1);
                decoderNum = rs.getString(2);
                role = rs.getString(4);

                allUsers.add(new User(username, decoderNum, null, role));
            }
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            JOptionPane.showMessageDialog(null, e
                    + "\nThere is a problem with the database connection or query");
            System.exit(0);
        }
        return allUsers;
    }

    /**
     * Method to authenticate a user logging in
     *
     * @param username the username of the user
     * @param password the password of the user
     * @return a user object
     */
    public User authnticateUser(String username, String password) {
        User myUser = null;
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            Statement s = conn.createStatement();

            ResultSet rs = s.executeQuery("SELECT username, decoder_num, user_pass, role FROM Customer WHERE username = \"" + username + "\" AND user_pass = \"" + password + "\"");

            String uname;
            String decoderNum;
            String role;

            while (rs.next()) {
                uname = rs.getString(1);
                decoderNum = rs.getString(2);
                role = rs.getString(4);

                myUser = new User(username, decoderNum, null, role);
            }
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            JOptionPane.showMessageDialog(null, e
                    + "\nThere is a problem with the database connection or query");
            System.exit(0);
        }
        return myUser;
    }

    /**
     * Method to remove a user from the database
     *
     * @param row the index of the selected row on the table
     */
    public void removeRow(int row) {
        try {
            try {
                Connection conn = null;
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = java.sql.DriverManager.getConnection(
                        "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

                PreparedStatement a = conn.prepareStatement("DELETE FROM Decoder_Status WHERE "
                        + "decoder_num=?");
                a.setString(1, (String) getValueAt(row, 0));
                a.execute();

                PreparedStatement b = conn.prepareStatement("DELETE FROM Charge WHERE "
                        + "decoder_num=?");
                b.setString(1, (String) getValueAt(row, 0));
                b.execute();

                PreparedStatement c = conn.prepareStatement("DELETE FROM Payment WHERE "
                        + "decoder_num=?");
                c.setString(1, (String) getValueAt(row, 0));
                c.execute();

                PreparedStatement p = conn.prepareStatement("DELETE FROM Customer WHERE "
                        + "decoder_num=?");

                p.setString(1, (String) getValueAt(row, 0));
                p.execute();

                JOptionPane.showMessageDialog(null, "Delete successful");

            } catch (ClassNotFoundException | InstantiationException |
                    IllegalAccessException | SQLException e) {
                JOptionPane.showMessageDialog(null, e
                        + "\nThere is a problem with the database connection or query");
                System.exit(0);
            }
            userRecords = new ArrayList<>();
            fetchTableData();
            fireTableRowsDeleted(row, row);
        } catch (ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(null, ex
                    + "\nThere is nothing to remove");
        }
    }

}
