package constantapp.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Constant
 */
public class DefaultingUsers extends AbstractTableModel {

    private final String[] columnNames;
    private ArrayList<User> userRecords;
    private ArrayList<User> userRecords1;
    static DefaultingUsers theModel = null;
    private String status;

    /**
     * Constructor for DefaultingUsers
     *
     * @param status the status of the user whether connected or disconnected
     */
    public DefaultingUsers(String status) {
        userRecords = new ArrayList<>();
        columnNames = new String[]{"Username", "Decoder Number"};
        this.status = status;
        fetchTableData(status);
    }

    public static DefaultingUsers getInstance(String status) {
        if (theModel == null) {
            theModel = new DefaultingUsers(status);
        }
        return theModel;
    }

    /**
     * Get the number of rows in the table model
     *
     * @return the number of rows of the table model
     */
    @Override
    public int getRowCount() {
        return userRecords.size();
    }

    /**
     * Get the number of columns in the table
     *
     * @return the number of columns in the table
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return userRecords.get(rowIndex).getUsername();
            case 1:
                return userRecords.get(rowIndex).getDecoderNum();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        try {
            return columnNames[columnIndex];
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Retrieve data from the database
     *
     * @param status the status of the user whether connected or disconnected
     */
    private void fetchTableData(String status) {
        try {

            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
                    "jdbc:mysql://localhost:3307/Final?user=root&password=Yomamaishere12");

            Statement s = conn.createStatement();

            ResultSet rs = s.executeQuery("SELECT Customer.username, Customer.decoder_num FROM Customer, Decoder_Status WHERE decoder_status= \"" + status + "\"");

            String username;
            String decoderNum;

            while (rs.next()) {
                username = rs.getString(1);
                decoderNum = rs.getString(2);

                if (status.equalsIgnoreCase("connected")) {

                }
                userRecords.add(new User(username, decoderNum, null, null));
            }
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            JOptionPane.showMessageDialog(null, e
                    + "\nThere is a problem with the database connection or query");
            System.exit(0);
        }
    }

}
