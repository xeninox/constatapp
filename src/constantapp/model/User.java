/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp.model;

/**
 *
 * @author Constant
 */
public class User {

    // instance variables
    private String username;
    private String decoderNum;
    private String password;
    private String role;

    /**
     * Default constructor
     */
    public User() {
    }

    /**
     * Constructor taking in parameters
     *
     * @param username the username of the user
     * @param decoderNum the decoder number
     * @param password password of the user
     * @param role role of the user
     */
    public User(String username, String decoderNum, String password, String role) {
        this.username = username;
        this.decoderNum = decoderNum;
        this.password = password;
        this.role = role;
    }

    /**
     * Method to get the username of the user
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Method to set the username of the user
     *
     * 
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Method to get the decoder number
     *
     * @return the decoder number
     */
    public String getDecoderNum() {
        return decoderNum;
    }

    /**
     * Method to set the decoder number
     *
     * @param decoderNum the decoder number
     */
    public void setDecoderNum(String decoderNum) {
        this.decoderNum = decoderNum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
