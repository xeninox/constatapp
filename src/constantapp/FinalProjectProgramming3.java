/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantapp;

import constantapp.controller.HomeViewController;
import constantapp.model.UserModel;
import constantapp.model.UserPaymentModel;
import constantapp.view.HomeView;


/**
 *
 * @author Constant
 */
public class FinalProjectProgramming3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UserModel um = UserModel.getInstance();
        UserPaymentModel uPayModel = UserPaymentModel.getInstance();
        HomeView hv = new HomeView();
        HomeViewController hmv = new HomeViewController(um, hv, uPayModel);
        hmv.handleUIControls();
    }

}
